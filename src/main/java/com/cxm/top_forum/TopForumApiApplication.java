package com.cxm.top_forum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopForumApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(TopForumApiApplication.class, args);
    }

}
