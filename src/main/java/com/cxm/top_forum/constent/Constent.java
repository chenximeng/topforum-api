package com.cxm.top_forum.constent;

/**
 * @author cxm
 * @date 2021/8/12
 */
public class Constent {
    public static final String SESSION_CAPTCHA = "captcha";
    public static final String SESSION_MAIL_VERIFY = "emailVerify";
    public static final String SESSION_PHONE_VERIFY = "phoneVerify";
    public static final String REDIS_TAG_LIST = "tagList";
}
