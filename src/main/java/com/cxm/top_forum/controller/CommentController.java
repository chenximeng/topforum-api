package com.cxm.top_forum.controller;

import com.cxm.top_forum.pojo.Comment;
import com.cxm.top_forum.pojo.RespBean;
import com.cxm.top_forum.pojo.req.AddCommentReq;
import com.cxm.top_forum.pojo.req.CommentReq;
import com.cxm.top_forum.service.CommentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author cxm
 * @date 2021/8/6
 */
@RestController
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @ApiOperation("获取评论")
    @GetMapping("/")
    public RespBean getComment(CommentReq req) {
        return RespBean.success(commentService.getComments(req.getArticleId()));
    }

    @ApiOperation("增加评论")
    @PostMapping("/")
    public RespBean addComment(@RequestBody AddCommentReq req){
        Comment comment = new Comment();
        comment.setContent(req.getContent());
        comment.setArticleId(req.getArticleId());
        comment.setUserId(req.getUserId());
        comment.setHasParent((short) 0);
        comment.setParentId(0L);
        comment.setCreateTime(new Date());
        commentService.save(comment);
        return RespBean.success("评论成功");
    }
}
