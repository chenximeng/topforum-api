package com.cxm.top_forum.controller;

import com.cxm.top_forum.constent.Constent;
import com.cxm.top_forum.pojo.RespBean;
import com.cxm.top_forum.pojo.Tag;
import com.cxm.top_forum.service.TagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author cxm
 * @date 2021/8/6
 */
@RestController
@Api("标签")
@RequestMapping("/tag")
public class TagController {
    @Autowired
    private TagService tagService;
    @Autowired
    private RedisTemplate redisTemplate;

    @ApiOperation("获取标签列表")
    @GetMapping("/list")
    public RespBean listTags(){
        ValueOperations ops = redisTemplate.opsForValue();
        Object o = ops.get(Constent.REDIS_TAG_LIST);
        if (o != null) {
            return RespBean.success(o);
        }
        List<Tag> list = tagService.list();
        ops.set(Constent.REDIS_TAG_LIST,list);
        return RespBean.success(list);
    }
}
