package com.cxm.top_forum.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author cxm
 * @date 2021/8/18
 */
@Data
@ApiModel("点赞关系")
public class Likes {
    @TableId(type = IdType.AUTO)
    private Long id;
    @ApiModelProperty("点赞用户id")
    private Long userId;
    @ApiModelProperty("被点赞的id")
    private Long parentId;
    @ApiModelProperty("1:帖子 2:评论")
    private Short parent;
}
