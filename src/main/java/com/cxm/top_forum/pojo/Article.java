package com.cxm.top_forum.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author cxm
 * @date 2021/8/6
 */
@ApiModel("帖子")
@Data
public class Article {
    @ApiModelProperty("id")
    @TableId(type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("title")
    private String title;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("点赞数")
    private Long likes;

    @ApiModelProperty("发布人id")
    private Long userId;

    @ApiModelProperty("标签id")
    private Long tagId;

    @ApiModelProperty("状态：0(不可见),1(可见)")
    private Short status;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "Asia/Shanghai")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "Asia/Shanghai")
    private Date updateTime;
}
