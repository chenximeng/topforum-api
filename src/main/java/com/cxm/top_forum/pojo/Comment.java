package com.cxm.top_forum.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author cxm
 * @date 2021/8/6
 */
@Data
@ApiModel("评论")
public class Comment {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("帖子id")
    private Long articleId;

    @ApiModelProperty("是否有父级评论：1(有),0(无)")
    private Short hasParent;

    @ApiModelProperty("父评论id")
    private Long parentId;

    @ApiModelProperty("点赞数量")
    private Long likes;

    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "Asia/Shanghai")
    private Date createTime;

    @ApiModelProperty("评论人id")
    private Long userId;

    @ApiModelProperty("内容")
    private String content;
}
