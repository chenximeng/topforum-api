package com.cxm.top_forum.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 响应bean
 *
 * @author cxm
 * @date 2021/8/12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RespBean {
    private Integer status;
    private Object data;
    private String msg;

    public static RespBean success() {
        return new RespBean(200,null,"");
    }

    public static RespBean success(String msg) {
        return new RespBean(200,null,msg);
    }

    public static RespBean success(Object data) {
        return new RespBean(200,data,"");
    }

    public static RespBean success(String msg, Object data) {
        return new RespBean(200, data, msg);
    }

    public static RespBean success(Integer status, String msg, Object data) {
        return new RespBean(status, data, msg);
    }

    public static RespBean fail() {
        return new RespBean(500,null,"请求失败");
    }

    public static RespBean fail(String msg) {
        return new RespBean(500,null,msg);
    }

    public static RespBean fail(Object data) {
        return new RespBean(200,data,"请求失败");
    }

    public static RespBean fail(String msg, Object data) {
        return new RespBean(500, data, msg);
    }

    public static RespBean fail(Integer status, String msg, Object data) {
        return new RespBean(status, data, msg);
    }
}
