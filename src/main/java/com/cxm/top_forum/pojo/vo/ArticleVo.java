package com.cxm.top_forum.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * @author cxm
 * @date 2021/8/13
 */
@Data
public class ArticleVo {
    private List<ArticlePage> list;
    private Long pageNum;
    private Long total;
}
