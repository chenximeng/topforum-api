package com.cxm.top_forum.pojo.vo;

import com.cxm.top_forum.pojo.Comment;
import lombok.Data;

import java.util.Date;

/**
 * @author cxm
 * @date 2021/8/20
 */
@Data
public class CommentPage extends Comment {
    private String username;
    private Date createTime;
}
