package com.cxm.top_forum.pojo.vo;

import com.cxm.top_forum.pojo.Article;
import lombok.Data;

/**
 * @author cxm
 * @date 2021/8/18
 */
@Data
public class ArticleItemVo extends Article {
    private String tagName;
    private Long commentNum;
    private String headUrl;
    private String username;
}
