package com.cxm.top_forum.pojo.vo;

import com.cxm.top_forum.pojo.Comment;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author cxm
 * @date 2021/8/20
 */
@Data
public class CommentVo extends Comment {
    private String username;
    private String headUrl;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "Asia/Shanghai")
    private Date createTime;
}
