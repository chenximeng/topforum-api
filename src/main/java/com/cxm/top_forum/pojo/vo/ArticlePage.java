package com.cxm.top_forum.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author cxm
 * @date 2021/8/12
 */
@Data
public class ArticlePage {
    private Long id;
    private String title;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "Asia/Shanghai")
    private Date createTime;
    private String tagName;
    private String headUrl;
    private String username;
    private Long likes;
}
