package com.cxm.top_forum.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author cxm
 * @date 2021/8/6
 */
@ApiModel("标签")
@Data
public class Tag {
    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("名字")
    private String tagName;
}
