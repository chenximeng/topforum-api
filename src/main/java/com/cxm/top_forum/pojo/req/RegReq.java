package com.cxm.top_forum.pojo.req;

import lombok.Data;

/**
 * @author cxm
 * @date 2021/8/12
 */
@Data
public class RegReq {
    private String username;
    private String email;
    private String phone;
    private String password;
    private String checkPass;
    private String verify;
}
