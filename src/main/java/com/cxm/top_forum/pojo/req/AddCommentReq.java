package com.cxm.top_forum.pojo.req;

import lombok.Data;

@Data
public class AddCommentReq {
    private String content;
    private Long userId;
    private Long articleId;
}
