package com.cxm.top_forum.pojo.req;

import lombok.Data;

/**
 * @author cxm
 * @date 2021/8/20
 */
@Data
public class CommentReq {
    private Long articleId;
    private Integer pageNum;
    private Integer pageSize;
}
