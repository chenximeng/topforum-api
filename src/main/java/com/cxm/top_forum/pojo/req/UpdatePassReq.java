package com.cxm.top_forum.pojo.req;

import lombok.Data;

@Data
public class UpdatePassReq {
    private String oldPass;
    private String newPass;
    private Long userId;
}
