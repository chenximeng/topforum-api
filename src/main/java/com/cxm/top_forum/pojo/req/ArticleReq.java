package com.cxm.top_forum.pojo.req;

import lombok.Data;

/**
 * @author cxm
 * @date 2021/8/13
 */
@Data
public class ArticleReq {
    private Integer id;
    private Integer tagId;
    private Integer pageNum;
    private Integer pageSize;
    private String keyword;
}
