package com.cxm.top_forum.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author cxm
 * @date 2021/8/6
 */
@Data
@ApiModel("用户")
public class User {
    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("是否为管理员：1(是),0(不是)")
    private Short admin;

    @ApiModelProperty("状态 1(启用),0(禁用)")
    private Short status;

    @ApiModelProperty("头像链接")
    private String headUrl;

    private Date createTime;
}
