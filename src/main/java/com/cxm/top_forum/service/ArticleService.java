package com.cxm.top_forum.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cxm.top_forum.pojo.Article;
import com.cxm.top_forum.pojo.Likes;
import com.cxm.top_forum.pojo.req.ArticleReq;
import com.cxm.top_forum.pojo.vo.ArticleItemVo;
import com.cxm.top_forum.pojo.vo.ArticlePage;
import com.cxm.top_forum.pojo.vo.ArticleVo;

/**
 * @author cxm
 * @date 2021/8/6
 */
public interface ArticleService extends IService<Article> {
    /**
     * 获取帖子
     * @param req
     * @return
     */
    ArticleVo listArticle(Page<ArticlePage> page, ArticleReq req);

    ArticleItemVo getWithTagById(Long id);

    void like(Likes likes);

    void disLike(Likes likes);
}
