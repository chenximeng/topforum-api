package com.cxm.top_forum.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cxm.top_forum.constent.Constent;
import com.cxm.top_forum.mapper.UserMapper;
import com.cxm.top_forum.pojo.RespBean;
import com.cxm.top_forum.pojo.User;
import com.cxm.top_forum.pojo.req.LoginReq;
import com.cxm.top_forum.pojo.req.RegReq;
import com.cxm.top_forum.service.UserService;
import com.cxm.top_forum.util.JwtUtil;
import com.cxm.top_forum.util.Md5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author cxm
 * @date 2021/8/6
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Value("${jwt.tokenHead}")
    String tokenHead;
    @Autowired
    private JwtUtil jwtUtil;

    @Override
    public RespBean login(LoginReq loginReq, HttpSession session) {
        if (!loginReq.getCaptcha().equals(session.getAttribute(Constent.SESSION_CAPTCHA))) {
            return RespBean.fail("验证码错误");
        }
        User user = userMapper.loadLoginUser(loginReq.getUsername());
        System.out.println(user);
        System.out.println(user.getStatus()==1);
        if (user == null) {
            return RespBean.fail("用户不存在");
        }

        if (!user.getPassword().equals(Md5Util.enCode(loginReq.getPassword()))) {
            return RespBean.fail("密码错误");
        }

        if (user.getStatus()==1){
            return RespBean.fail("账号已被禁用");
        }

        Map<String, Object> data = new HashMap<>();
        String token = jwtUtil.generateToken(user);
        data.put("token",token);
        data.put("tokenHead",tokenHead);
        data.put("user",user);

        session.removeAttribute(Constent.SESSION_CAPTCHA);

        return RespBean.success("登录成功",data);
    }

    @Override
    public boolean isEmailExist(String email) {
        User user = userMapper.getUserByEmail(email);
        if (user == null) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isNameExist(String username) {
        User user = userMapper.getUserByName(username);
        if (user == null) {
            return false;
        }
        return true;
    }

    @Override
    public void register(RegReq req) {
        User user = new User();
        user.setAdmin((short) 0);
        user.setStatus((short) 0);
        user.setUsername(req.getUsername());
        user.setCreateTime(new Date());
        user.setEmail(req.getEmail());
        user.setPhone(req.getPhone());
        user.setHeadUrl("https://pic4.zhimg.com/80/v2-74067c7bc04ec1d2842143cd3691dd74_720w.jpg");
        user.setPassword(Md5Util.enCode(req.getPassword()));
        userMapper.insert(user);
    }


}
