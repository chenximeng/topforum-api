package com.cxm.top_forum.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cxm.top_forum.mapper.TagMapper;
import com.cxm.top_forum.pojo.Tag;
import com.cxm.top_forum.service.TagService;
import org.springframework.stereotype.Service;

/**
 * @author cxm
 * @date 2021/8/6
 */
@Service
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag> implements TagService {
}
