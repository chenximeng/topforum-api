package com.cxm.top_forum.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cxm.top_forum.mapper.CommentMapper;
import com.cxm.top_forum.pojo.Comment;
import com.cxm.top_forum.pojo.req.CommentReq;
import com.cxm.top_forum.pojo.vo.CommentVo;
import com.cxm.top_forum.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author cxm
 * @date 2021/8/6
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {
    @Autowired
    private CommentMapper commentMapper;

    @Override
    public List<CommentVo> listComment(Page<Comment> page, CommentReq req) {
        return null;
    }

    @Override
    public List<CommentVo> getComments(Long articleId) {
        return commentMapper.getComments(articleId);
    }
}
