package com.cxm.top_forum.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cxm.top_forum.pojo.RespBean;
import com.cxm.top_forum.pojo.User;
import com.cxm.top_forum.pojo.req.LoginReq;
import com.cxm.top_forum.pojo.req.RegReq;

import javax.servlet.http.HttpSession;

/**
 * @author cxm
 * @date 2021/8/6
 */
public interface UserService extends IService<User> {

    /**
     * 登录
     * @param loginReq
     * @return
     */
    RespBean login(LoginReq loginReq, HttpSession session);

    boolean isEmailExist(String email);

    boolean isNameExist(String username);

    void register(RegReq req);
}
