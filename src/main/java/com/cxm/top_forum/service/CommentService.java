package com.cxm.top_forum.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cxm.top_forum.pojo.Comment;
import com.cxm.top_forum.pojo.req.CommentReq;
import com.cxm.top_forum.pojo.vo.CommentVo;

import java.util.List;

/**
 * @author cxm
 * @date 2021/8/6
 */
public interface CommentService extends IService<Comment> {
    List<CommentVo> listComment(Page<Comment> page, CommentReq req);

    List<CommentVo> getComments(Long articleId);
}
