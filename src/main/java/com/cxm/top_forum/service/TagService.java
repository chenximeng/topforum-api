package com.cxm.top_forum.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cxm.top_forum.pojo.Tag;

/**
 * @author cxm
 * @date 2021/8/6
 */
public interface TagService extends IService<Tag> {
}
