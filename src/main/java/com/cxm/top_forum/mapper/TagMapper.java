package com.cxm.top_forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxm.top_forum.pojo.Tag;

/**
 * @author cxm
 * @date 2021/8/6
 */
public interface TagMapper extends BaseMapper<Tag> {
}
