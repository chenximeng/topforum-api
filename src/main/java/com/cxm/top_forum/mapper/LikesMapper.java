package com.cxm.top_forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxm.top_forum.pojo.Likes;
import org.apache.ibatis.annotations.Param;

/**
 * @author cxm
 * @date 2021/8/18
 */
public interface LikesMapper extends BaseMapper<Likes> {
    Likes getLike(@Param("userId") Long userId, @Param("parentId") Long parentId);
}
