package com.cxm.top_forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxm.top_forum.pojo.User;

import java.util.List;

/**
 *
 * @author cxm
 * @date 2021/8/6
 */
public interface UserMapper extends BaseMapper<User> {
    List<User> listUser();

    /**
     * 根据用户名、电话或邮箱获取用户
     * @param keyword
     * @return
     */
    User loadLoginUser(String keyword);

    User getUserByEmail(String email);

    User getUserByName(String username);
}
