package com.cxm.top_forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cxm.top_forum.pojo.Article;
import com.cxm.top_forum.pojo.req.ArticleReq;
import com.cxm.top_forum.pojo.vo.ArticleItemVo;
import com.cxm.top_forum.pojo.vo.ArticlePage;
import org.apache.ibatis.annotations.Param;

/**
 * @author cxm
 * @date 2021/8/6
 */
public interface ArticleMapper extends BaseMapper<Article> {

    /**
     * 获取帖子
     * @param page
     * @param req
     * @return
     */
    IPage<ArticlePage> listArticle(Page<ArticlePage> page, @Param("req") ArticleReq req);

    ArticleItemVo getWithTagById(Long id);

}
