package com.cxm.top_forum.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cxm.top_forum.pojo.Comment;
import com.cxm.top_forum.pojo.vo.CommentVo;

import java.util.List;

/**
 * @author cxm
 * @date 2021/8/6
 */
public interface CommentMapper extends BaseMapper<Comment> {
     List<CommentVo> getComments(Long articleId);
}
