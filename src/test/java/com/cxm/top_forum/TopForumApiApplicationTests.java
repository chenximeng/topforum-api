package com.cxm.top_forum;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cxm.top_forum.mapper.ArticleMapper;
import com.cxm.top_forum.mapper.UserMapper;
import com.cxm.top_forum.pojo.req.ArticleReq;
import com.cxm.top_forum.pojo.vo.ArticlePage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
class TopForumApiApplicationTests {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Test
    void contextLoads() {
        Page<ArticlePage> page = new Page<>(3,1);
    }

    @Test
    void testReid(){
        Page<ArticlePage> page = new Page<>(1,10);
        ArticleReq req = new ArticleReq();
        IPage<ArticlePage> iPage = articleMapper.listArticle(page, req);
        for (ArticlePage record : iPage.getRecords()) {
            System.out.println(record);
        }
    }

}
